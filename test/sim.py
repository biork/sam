
"""
Simulate (naïvely!) mutational processes on a (by default) diploid genome
with a specifiable number of chromosomes and chromosome sizes followed by
next-gen sequencing read generation and alignment.

This script's goal is the generation of simulated data for sanity-checking
_basic correctness_ of computational tools' handling of sequencing data and,
in particular, coordinate to sequence relations. It should, for example, be
easy to catch off-by-one errors with this script.

Constraints can be added to insure the existence in otherwise random
sequence data of classes of variation for the purpose of testing other code.

It is _not_ intended to test:
1. statistical methods
2. mutational models
3. scalability. (It generates small-ish simulated data.)

The overriding goal is simplicity of use for sanity checking with genuinely
random data.

ASSUMES:
1. bwa is in your PATH
2. samtools is in your PATH

Data NOT yet simulated:
1. Errors in the sequencing; reads are currently perfect.
2. Quality measures. All generated FASTA have quality 'I'
"""

from sys import argv,stdin,stdout,stderr,exit
import argparse
from os import environ
from re import split as re_split, match as re_match
from random import uniform,choice,random,randrange,gauss
from subprocess import run as run_child
from warnings import warn
from os.path import join as path_join, isdir
from os import mkdir
from collections import namedtuple

FragSpec = namedtuple("FragSpec",["contig","homolog","offset","length"])

_DEFAULT_BASE_QUALITY = "I"
#_HAP_ENC = ("mat","pat")

# Incorporate environment variables.

if "BASE_P" in environ:
	_BASES_P = [ float(v) for v in re_split( "[ ,/]", environ("BASE_P"), 3 ) ]
else:
	_BASES_P = [0.25,0.25,0.25,0.25]

_BASES_CUM_P = [ sum(_BASES_P[:i]) for i in range(len(_BASES_P)) ]
_BASE_SUM_P = sum(_BASES_P)


# Define and parse command-line arguments

def _emit_summary( args, fp ):
	"""
	Simply summarize all arguments in prose.
	"""
	print( "# \"{}\" has {} chromosomes with total nucleotide length {}".format(
		args.ref,
		args.contig_count,
		args.genome_length ),
		file=fp )
	print( "# Fragment lengths are distributed as N({},{})".format( *args.tlen ),
		file=fp )
	print( "# {}-end read lengths are distributed as N({},{})".format( 
		"Paired" if not args.single_end else "Single", *args.rdlen ),
		file=fp )
	print( "# \"{}\" is sequenced {}".format( 
		args.subject,
		"with exactly {} reads".format(args.rdnum) 
			if args.depth is None 
			else "to an expected mean depth of {}".format(args.depth) ),
		file=fp )
	# TODO: Also summarize in machine-readable form for downstream scripts.


class SplitCommaSepInts(argparse.Action):
	EXC_MSG = "requires exactly 2 comma-separated integers"
	def __init__( self, option_strings, dest, nargs=None, **kwargs ):
		"""
		const=None, default=None, type=None, choices=None, required=False, help=None, metavar=None
		"""
		#print( "SplitCommaSepInts.__init__(", option_strings, dest, nargs, ','.join( "{}={}".format( k, v ) for k,v in kwargs.items() ), ")" )
		if nargs is not None:
			raise ValueError("nargs not allowed")
		super(SplitCommaSepInts, self).__init__(option_strings, dest, **kwargs)

	def __call__(self, parser, namespace, values, option_string=None):
		#print('SplitCommaSepInts.__call__( %r %r %r)' % (namespace, values, option_string))
		try:
			value_list = [ int(v) for v in values.split(',') ]
		except ValueError as e:
			# 'from None' _replaces_ the original cause with our more specific msg
			raise ValueError("{} {}".format( option_string, SplitCommaSepInts.EXC_MSG ) ) from None
		if option_string in ['-T','--tlen','-R','--rdlen'] and len(value_list) > 2:
			raise ValueError("{} {}".format( option_string, SplitCommaSepInts.EXC_MSG ) )
		setattr( namespace, self.dest, value_list )


############################################################################

class Complementer:
	def __getitem__( self, i ):
		assert isinstance(i,int)
		upcase = i&(~(0b100000))
		if   upcase == 65: # ord('A'):
			ch = 'T'
		elif upcase == 67: # ord('C'):
			ch = 'G'
		elif upcase == 71: # ord('G'):
			ch = 'C'
		else:
			ch = 'A'
		return ch

_complementer = Complementer()

def _revcomp( seq ):
	return ''.join(reversed(seq.translate(_complementer)))

def _clamp( value, max_value ):
	return min( value, max_value )

def _random_base():
	"""
	Draw a random base given the (not necessarily uniform) probability masses
	in the _BASES_P.
	"""
	global _BASE_SUM_P,_BASES_CUM_P
	rn = uniform(0,_BASE_SUM_P)
	for i in range(len(_BASES_CUM_P)-1,0,-1):
		if rn > _BASES_CUM_P[i]:
			return "ACGT"[i]
	return "A"

def _print_fasta( seq, name="random", width=70, dest=stdout ):
	print( ">{}".format( name ), file=dest )
	i = 0
	while i < len(seq):
		line_len = min(width,len(seq)-i)
		print( "".join( seq[i:(i+line_len)] ), file=dest )
		i += line_len


def _generate_variation( args, genome ):
	"""
	Generate and apply mutations according to supplied statistical parameters.
	num SNVs (het-ref, het-alt, hom-alt)
	num indels
	"""
	variants = list()
	# TODO: These are currently het-ref by default (unless a SNV _happens_
	# to hit both sequences of a given position).
	for snv in range(round(args.snvs if args.snvs >= 1.0 else args.snvs*args.genome_length)):
		contig = randrange( args.contig_count )
		# Hit _either_ of homologous chromosomes unless mutations are to be
		# confined to one particular homolog
		homolog = int( random() < 0.5 ) if args.haploid is None else args.haploid
		offset = randrange( args.contig_lengths[contig] )
		ref_base = genome[contig][homolog][offset]
		alternates = set("ACGT")
		alternates.discard(ref_base)
		alt_base = choice( list(alternates) )
		genome[contig][homolog][offset] = alt_base
		variants.append( ( contig, homolog, offset+1, ref_base, alt_base ) )

	variants.sort()
	with open( path_join( args.wdir, 'variants.tab' ), "w" ) as fp:
		for v in variants:
			print( *v, sep="\t", file=fp )


def _random_fragment( args, subject ) -> (str,FragSpec):
	"""
	Generate a random fragment respecting
	1. distribution constraints on fragment length and
	2. other constraints on fragment location.
	Return the fragment (forward) sequence verbatim and the 0-based, half-open
	specification.
	"""
	# Use reference chromosome length, though indels may have changed it!
	# Choose random contig, homolog, position and fragment length.
	contig = randrange( args.contig_count ) if args.mzone is None else args.mzone[0]
	homolog = int( random() < 0.5 ) if args.haploid is None else args.haploid
	tlen = round( gauss( *args.tlen ) )
	contig_length = len(subject[contig][homolog])
	start_offset = randrange( max( 1, contig_length - tlen ) )

	# Generate the fragment from which read(s) will be taken.
	flen = _clamp( tlen, contig_length - start_offset )
	fragment = "".join( subject[contig][homolog][start_offset:(start_offset+flen)] )
	return (fragment, FragSpec( contig, homolog, start_offset, flen ))


def _simulate_sequencing( args, subject, fq_fp ):
	"""
	Simulate either single- or paired-end reads according to whether the file
	argument is a single file handle or a tuple pair of file handles.
	Generates one or two FASTQ files.
	Notes:
	coverage = n_reads * rd_len / genome_size
	n_reads = coverage * genome_size / rd_len
	"""

	# How many templates are actually required? Either an absolute count has
	# been supplied or we calculate a count from a target depth.

	paired_end = isinstance(fq_fp,tuple)
	N = args.rdnum if args.depth is None else \
			round(
				args.depth*args.genome_length
				/ min(args.tlen[0],args.genome_length) )

	random_rdlen = lambda L:_clamp( L, round( gauss( *args.rdlen ) ) )
	# TODO: Control in paired-end case whether or not they overlap?

	# Preceding insures that target depth is achieved exactly in the (unusual)
	# case that read lengths are greater than a chromosome length.

	# Generate fragments, then reads from the fragments.

	format_bounds = lambda b:"[{},{}]({}bp)".format( *b, b[1]-b[0]+1 )

	with open( path_join( args.wdir, 'fragments.tab' ), "w" ) as fragments_fp:

		for i in range(N):

			fragment,spec = _random_fragment( args, subject )
			print( spec.contig, spec.homolog,
				format_bounds( ( spec.offset+1, spec.offset + spec.length ) ),
				sep="\t", end="\t", file=fragments_fp ) # ...finished below!

			# Now generate read(s) from the template


			rdlen = random_rdlen( len(fragment) )

			if paired_end: # paired-end reads
				# 1st
				closed_bounds = ( spec.offset                  +1, spec.offset + rdlen )
				print( format_bounds(closed_bounds), sep="\t", end="\t", file=fragments_fp )
				print( "@SIMRD{:08X}\n{}\n+\n{}".format( i,
					fragment[:rdlen],
					_DEFAULT_BASE_QUALITY*rdlen), file=fq_fp[0] )

				# 2nd
				rdlen = random_rdlen( len(fragment) )
				closed_bounds = ( spec.offset+spec.length-rdlen+1, spec.offset + spec.length )
				print( format_bounds(closed_bounds), sep="\t", file=fragments_fp )
				print( "@SIMRD{:08X}\n{}\n+\n{}".format( i,
					_revcomp( fragment )[:rdlen],
					_DEFAULT_BASE_QUALITY*rdlen), file=fq_fp[1] )
			else: # single-ended...
				# Just one or the other _end_.
				closed_bounds = ( spec.offset                  +1, spec.offset + rdlen )
				print( format_bounds(closed_bounds), sep="\t", file=fragments_fp )
				print( "@SIMRD{:08X}\n{}\n+\n{}".format( i,
					fragment[:rdlen] if random() < 0.5 else _revcomp( fragment[:rdlen] ),
					_DEFAULT_BASE_QUALITY*rdlen), file=fq_fp )
			

def _generate_bam( genome_path, args, fastq:list, stderr_fp ):
	"""
	This merely encapsulate running of bwa followed by samtools.
	"""
	assert isinstance(fastq,list),"...so it's concatenatable to argument list"

	cmd = [ "bwa","index", genome_path ]

	if __debug__:
		print( *cmd, file=stderr )

	run_child( cmd, stderr=stderr_fp, check=True)

	samfile = path_join( args.wdir, "{}.sam".format( args.subject ) )
	bamfile = path_join( args.wdir, "{}.bam".format( args.subject ) )

	cmd = [ "bwa", "mem", "-a", "-o", samfile, genome_path ] + fastq
	if __debug__:
		print( *cmd, file=stderr )
	run_child( cmd, stderr=stderr_fp, check=True )

	# ...and, finally, sort the alignments and convert to BAM.
	cmd = [ "samtools", "sort","-o", bamfile, samfile ]
	if __debug__:
		print( *cmd, file=stderr )
	run_child( cmd, stderr=stderr_fp, check=True )

	cmd = [ "samtools", "index", bamfile ]
	if __debug__:
		print( *cmd, file=stderr )
	run_child( cmd, stderr=stderr_fp, check=True )


def _main( args ):

	# Create the working directory if required, and open any output file(s) that
	# must be held open.

	if not isdir( args.wdir ):
		mkdir( args.wdir )

	if len(args.summary) > 0:
		with open( path_join( args.wdir, args.summary ), "w" ) as fp:
			_emit_summary( args, fp )
	else:
		_emit_summary( args, stderr )

	# Create a random haploid genome...
	haploid_ref = [ [ _random_base() for i in range(clen) ] for clen in args.contig_lengths ]
	# ...and an entirely homozygous-reference diploid instance thereof.
	haploid_ref_filepath  = path_join( args.wdir, "{}.fa".format( args.ref ) )

	# Emit the "reference" genome (for later use by bwa).

	with open( haploid_ref_filepath, "w" ) as fp:
		for i,chromosome in enumerate(haploid_ref):
			_print_fasta( chromosome, name="{}".format(i), dest=fp )

	# Create two "perfect" instantiations of the haploid reference representing
	# a diploid homozygous-reference individual...
	diploid_genome = [ ( chromo.copy(), chromo.copy() ) for chromo in haploid_ref ]
	# ...then generate random variations in it.
	_generate_variation( args, diploid_genome )

	with open( path_join( args.wdir, "{}.fa".format( args.subject ) ), "w" ) as fp:
		for i,chromosome in enumerate(diploid_genome):
			for hap in range(2):
				_print_fasta( chromosome[hap], name="chr{:02d}:homolog{:d}".format(i,hap), dest=fp )

	fastq = [ path_join( args.wdir, "{}.fq".format(i) ) for i in ((1,) if args.single_end else (1,2)) ]

	if args.single_end:
		with open( fastq[0], "w" ) as fp:
			_simulate_sequencing( args, diploid_genome, fp )
	else:
		with open( fastq[0], "w" ) as fp1, open( fastq[1], "w" ) as fp2:
			_simulate_sequencing( args, diploid_genome, (fp1,fp2) )

	# Align the generated reads.

	with open( args.tools_errors, "w" ) as fp:
		_generate_bam( haploid_ref_filepath, args, fastq, fp )

############################################################################

parser = argparse.ArgumentParser( description="Read simulator",
epilog="""Note that FILENAME arguments may also be "/dev/null" or "/dev/stderr".
Contigs/chromosomes are named by 0-based integers by default.""" )

# Implementation notes:
# 1. The action objects *are* instantiated but not __call__'ed unless the
#		corresponding arguments are provided--that is, they are NOT CALLED for
#		default arguments.
# 2. Thus, default arguments should be given in the form they will be used
#		by subsequent code--that is, NOT as comma-separated strings that still
#		need to be parsed.
# 3. Moreover, they should NOT be typed (using the type argument to
#		add_argument) as that merely triggers unnecessary and wrong type
#		checking and coercion by the parser.
#	4. A small complication in all this, possibly resolvable by more digging
#		into argparse' capabilities, is that the %(default) in help arguments
#		will be whatever Python thinks is an appropriate display.

HELP_SEQ = "Name for {} genome. This will be the base filename of multiple output files. [\"%(default)s\"]"

parser.add_argument( "-r", "--ref", type=str, default="genome",
	help=HELP_SEQ.format("REFERENCE") )
parser.add_argument( "-s", "--subject", type=str, default="subject",
	help=HELP_SEQ.format("SIMULATED") )
parser.add_argument( "-l", "--lengths", default=[1000,], action=SplitCommaSepInts, dest='contig_lengths',
	help="Comma-separated sequence of chromosome length(s).[%(default)s]" )
parser.add_argument( "-v", "--snvs", type=float, default=1.0,
	help="Either an absolute count of SNVs (if >= 1) or a frequency (per basepair) if (< 1.0). [%(default)f]" )
parser.add_argument( "-i", "--indels", type=float, default=0.0,
	help="Either an absolute count of indels (if >= 1) or a frequency (per basepair) if (< 1.0). [%(default)f]" )

HELP_INTERVAL = "Interval specified as contig:start-end (/([0-9]+):([0-9]+)-([0-9]+)/) {}.[\"%(default)s\"]"

parser.add_argument( "-m", "--mzone", default=None,
	help=HELP_INTERVAL.format( "containing all mutations" ) )
parser.add_argument( "-H", "--haploid", default=None, type=int, choices=[0,1], metavar="HOMOLOG",
	help="Confine all variation _and_ fragment generation to the same %(metavar)s of {%(choices)s} parent [%(default)s]" )
parser.add_argument( "-D", "--rdnum", type=int, default=1,
	help="Absolute read count. [%(default)d]" )
parser.add_argument( "-d", "--depth", type=int, default=None,
	help="(Simulated) coverage depth. Takes priority over rdnum, if supplied." )

HELP_GAUSS="Integer (mu,sigma) of normally-distributed simulated {} lengths. [N%(default)s]"

parser.add_argument( "-T", "--tlen",  default=[350,30], action=SplitCommaSepInts,
	help=HELP_GAUSS.format("template") )
parser.add_argument( "-R", "--rdlen", default=[150,20], action=SplitCommaSepInts,
	help=HELP_GAUSS.format("read") )
parser.add_argument( "-I", "--intersect", default=None,
	help=HELP_INTERVAL.format( "which every fragment must cover" ) )

parser.add_argument( "-W", "--wdir", type=str, default='output',
	help="Working directory (for all generated stuff) [\"%(default)s\"]" )

HELP_EXTRA_FILES="""Write the {} to %(metavar)s, if provided, otherwise
to stderr. [\"%(default)s\"]."""

parser.add_argument( "--summary", default="params.txt", metavar="FILENAME",
	help=HELP_EXTRA_FILES.format("arguments summary") )

parser.add_argument( "--tools_errors", default="bwa_and_samtools_stderr.txt",
	metavar="FILENAME",
	help=HELP_EXTRA_FILES.format("bwa and samtools stderr") )

parser.add_argument( "--single-end", action="store_true",
	help="Generate single-end reads. Default is paired." )

_args = parser.parse_args()

# Transform some arguments and calculate derived attributes.

# Precalculate the total genome size since it's needed in multiple places.

setattr(_args,"genome_length", sum(_args.contig_lengths) )
setattr(_args,"contig_count",  len(_args.contig_lengths) )

# Parse mutation zone into the numeric tuple: (contig,start,end)

if _args.mzone:
	m = re_match( r'(?P<contig>\d+):(?P<start>\d+)-(?P<end>\d+)', _args.mzone )
	if m is None or len(m.groups()) != 3:
		raise ValueError("--mzone argument must match /([0-9]+):([0-9]+)-([0-9]+)/")
	_args.mzone = (
		int(m.group('contig')),
		int(m.group('start')),
		int(m.group('end')) )

_main( _args )

