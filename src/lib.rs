//! Parse short-read sequence alignment records from their text ("SAM")
//! format, returning a struct representing an individual record's content.
//!
//! Provides a Rust struct representing the content of a SAM format
//! alignment.
//!
//! # Conventions (_very_ important to  understanding this API)
//!
//! 1. Every use of the term "position" in these documents refers to 1-based
//!		sequence positions. Every use of the term "offset" refers to a 0-based
//!		offset from some benchmark position. The first basepair of a sequence
//!		is at _offset 0_ and _position 1_.
//!	2. "Reference position" _always_ means in the _original_ reference
//!		_without_ the variation, _not_ in the reference _with_ the variation.

pub mod cigar;
pub mod text;
pub mod flags;

/// A 0-based offset.
///
/// This type is really just a marker that a variable is a 0-based
/// _offset_ rather than a (1-based) _position_.
type Goff = u32;

/// A 1-based position.
///
/// This type is really just a marker that a variable is a 1-based
/// _position_ rather than a (0-based) _offset_.
type Gpos = u32;

pub trait Alignment {

	fn cigar( &self ) -> &[cigar::Op];

	// These methods likely require different implementations for SAM (text)
	// files and BAM (binary) files.

	fn endpos( &self ) -> Gpos;

	/// Return the 1-based reference position corresponding to a (0-based)
	/// query offset.
	///
	/// A reference position within a deletion or reference padding will
	/// yield the cigar::Op specifying the variation.
	fn ref_position_of( &self, query_offset : u32 ) -> Result<Goff,Option<u8>>;

	/// Return the 0-based query offset corresponding to a (1-based) ref
	/// position.
	///
	/// A query offset within a deletion will yield the cigar opcode
	/// specifying the variation.
	fn query_offset_of( &self, ref_position : Gpos ) -> Result<u32,Option<u8>>;

	fn query_slice( &self, off : u32, len : u32 ) -> Vec<u8>;

	// All failures, out-of-bounds or unmappable position, yield None
	// without further ado.

	/// Retrieves the nucleotide at the given 0-based offset into the query
	/// sequence.
	/// No bounds checking is performed: out of bounds will cause panic.
	fn base_at_query_offset( &self, query_offset : u32 ) -> u8;

	/// Retrieves the quality score for the nucleotide at the given 0-based
	/// offset into the query sequence.
	/// No bounds checking is performed: out of bounds will cause panic.
	fn qual_at_query_offset( &self, query_offset : u32 ) -> u8;

	/// Returns the nucleotide in the aligned sequence at the given
	/// reference _position_ or the alignment (CIGAR) operation (always
	/// one of {'D','N'}) explaining its absence.
	///
	/// Notice:
	/// 1. this code does not have access to the actual reference so the
	/// 	base returned is necessarily _inferred_ from the query sequence.
	/// 2. "ref_position" means in the reference's _original_ coordinate
	///		system, _not_ the coordinate system with indels applied. So:
	///		a. This and #1 means we can't know what _was_ in a deleted position.
	///		b. We _do_ know what is in the position of an insert, but insertions
	///			do not _correspond_ to positions existing in the _original_
	///			coordinate system.
	fn base_at_ref_position( &self, ref_position : Gpos ) -> Result<u8,Option<u8>>;

	/// Returns the quality score for the nucleotide in the aligned sequence
	/// at the given reference _position_ or the CIGAR op explaining its
	/// absence.
	///
	/// All commentary re: base_at_ref_position applies here, too.
	fn qual_at_ref_position( &self, ref_position : Gpos ) -> Result<u8,Option<u8>>;

	// END OF REQUIRED METHODS

	/// Returns the sum of all CIGAR ops' reference consumptions.
	///
	/// This should correspond to the sum of M/D/N/=/X opcodes which are the
	/// only opcodes that consume reference sequence.

	fn ref_base_count( &self ) -> u32 {
		self.cigar().iter().map( |op| op.dr ).sum()
	}

	/// Returns the sum of all CIGAR ops' query consumptions.
	///
	/// This should correspond to the sum of M/I/S/=/X opcodes which are the
	/// only opcodes that consume query sequence.

	fn query_base_count( &self ) -> usize {
		self.cigar().iter().map( |op| op.dq as usize ).sum()
	}
}

