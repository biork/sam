
use std::fmt;
use std::fmt::Display;

/// CIGAR string opcodes' alphabetic representations.

const ALPHABETIC_OPCODE : [char;9] = [ 'M','I','D','N','S','H','P','=','X'];

// Following are the opcode defined for binary (BAM) files and intended to
// fit inside a nibble.

pub const OP_M : u8 = 0;
pub const OP_I : u8 = 1;
pub const OP_D : u8 = 2;
pub const OP_N : u8 = 3;
pub const OP_S : u8 = 4;
pub const OP_H : u8 = 5;
pub const OP_P : u8 = 6;
pub const OP_E : u8 = 7;
pub const OP_X : u8 = 8;

////////////////////////////////////////////////////////////////////////////

/// Represents one of the CIGAR opcodes and the number of query and/or
/// reference bases "consumed" by the operation in a particular CIGAR
/// string.
#[derive(Clone,Copy)]
pub struct Op {

	/// The operation as defined for the _binary_ (BAM) format.
	/// Use of the binary opcode is intended to support possible future
	/// handling of BAM format here; otherwise direct use of a char (or
	/// even ASCII u8 value) might be preferable.
	pub code:u8,

	/// The number of query bases "consumed" by this operation.
	///
	/// Split reads can be widely separate; u16 will not suffice.
	pub dq:u32,

	/// The number of reference bases "consumed" by this operation.
	pub dr:u32
}


impl Op {

	pub fn new( n: u32, code: u8 ) -> Self {
		match code {
			OP_M|OP_E|OP_X => Op{ code, dq:n, dr:n },
			OP_I|OP_S      => Op{ code, dq:n, dr:0 },
			OP_D|OP_N      => Op{ code, dq:0, dr:n }, 
			OP_H|OP_P      => Op{ code, dq:0, dr:0 }, 
			 _    => panic!("unexpected opcode")
		}
	}

	/// Returns the number of bases, reference or query, consumed by this op.

	/// Either the same number of reference _and_ query bases are consumed
	/// (e.g. for 'M','X' and '=' operations) or n bases are consumed from
	/// one and 0 from the other.

	pub fn n( &self ) -> u32 {
		// TODO: Optimize: might self.dq.max( self.dr ) be faster?
		if self.dq > 0 { self.dq } else { self.dr }
	}
}


/// Encapsulates coordinate calculations based on OpSeq operations.
///
/// Note that all coordinates handled by OpSeq methods are 0-based offsets
/// in the alignment's *local coordinate system* (relative to the first
/// base of the alignment) because an OpSeq has *no knowledge* of absolute
/// reference coordinates. This applies, in particular, to the
/// implementations of offset_in_query and offset_in_ref for which both
/// argument _and_ return values are _offsets_.
/// 
/// See the schematic in doc/indel.png.

impl Display for Op {

	fn fmt( &self, f: &mut fmt::Formatter) -> fmt::Result {
		write!( f, "{0:}{1:}", self.n(), ALPHABETIC_OPCODE[self.code as usize] )?;
		Ok(())
	}
}


/// An efficient char-to-BAM opcode converter.
//                           6 6 6 6 6 6 6 6 6 7 ...
//                           1 2 3 4 5 6 7 8 9 0 ...
//                           = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X
const X : u8 = 255;
const ALPHA2OP : [u8;28] = [ 7,X,X,X,X,X,X,2,X,X,X,5,1,X,X,X,0,3,X,6,X,X,4,X,X,X,X,8 ];

/// Convert a standard CIGAR string into a Vec<Op>.
///
/// This will panic if any opcode in the string is invalid, however, only
/// invalid opcode are currently detected. Other violations of the spec,
/// internal hard padding for example, are not detected.
///
/// Notice this dispenses with any dependency on a regular expressions
/// library. The CIGAR format is trivial; regular expressions are overkill.
pub fn from_str( cigar : &str ) -> Option<Vec<Op>> {

	Some(
        if cigar == "*" {
            Vec::<Op>::new()
        } else {
            cigar.chars()
                .scan( 0, | st, c |
                        if c.is_numeric() {
                            *st = 10*(*st) + (c as u32) - 0x30;
                            Some((0,'\0'))
                        } else {
                            let n = *st;
                            *st = 0;
                            Some((n,c))
                        } )
                .filter( |t| t.0 != 0 )
                .map( |t| Op::new( t.0, ALPHA2OP[t.1 as usize - 61] ) )
                .collect()
        })
}


/// Determines the offset in SEQ corresponding to the given offset
/// _from the alignment position_ in the reference, _if any_.
///
/// If the reference position is in a deletion ('D') or padding ('N'),
/// then there _is no corresponding position in the query_, and the
/// function returns the CIGAR u8 opcode as an error.
/// It returns Err(None) for simple out-of-bounds conditions.
///
/// Notice that out-of-bounds conditions can only occur _above_ the
/// alignment, never below it, because offset is non-negative and with
/// respect to the the alignment position.

pub fn offset_in_query( cigar: &[Op], off_in_ref:u32 ) -> Result<u32,Option<u8>> {

	let mut q:u32 = 0;
	let mut r:u32 = 0;

	for op in cigar {

		// Is this op consuming the reference offset?

		if r <= off_in_ref && off_in_ref < ( r + op.dr ) {
			match op.code {
				OP_M|OP_E|OP_X => return Ok(q+(off_in_ref-r)),
				OP_I|OP_S      => unreachable!(),
				OP_D|OP_N      => return Err(Some(op.code)),
				 _             => () // 'H', 'P' consume neither!
			}
		}
		q += op.dq;
		r += op.dr;
	}
	Err(None)
}

/// Determines the offset _from the alignment position_ in the reference
/// corresponding to the given offset in SEQ, _if any_.
///
/// If the query position is in an insertion ('I') or clipping ('S'),
/// then there _is no corresponding position in the reference_, and the
/// function returns the CIGAR u8 opcode as an error.
/// It returns Err(None) for simple out-of-bounds conditions.
///
/// Notice that out-of-bounds conditions can only occur _above_ the
/// alignment, never below it, because offset is non-negative and with
/// respect to the the alignment position.
pub fn offset_in_ref( cigar: &[Op], off_in_qry:u32 ) -> Result<u32,Option<u8>> {

	let mut q:u32 = 0;
	let mut r:u32 = 0;

	for op in cigar {

		// Is this op consuming query?

		if q <= off_in_qry && off_in_qry < ( q + op.dq ) {
			match op.code {
				OP_M|OP_E|OP_X => return Ok(r+(off_in_qry-q)),
				OP_I|OP_S      => return Err(Some(op.code)),
				OP_D|OP_N      => unreachable!(),
				 _             => ()
			}
		}
		q += op.dq;
		r += op.dr;
	}
	Err(None)
}


#[cfg(test)]
mod tests {

  use super::*;

	#[test]
	fn one_deletion() {

		//    0123456 Offset in ref (from alignment POS, not ref beginning!)
		//    |||  ||
		//    012  34 Offset in query
		//    MMMDDMM Cigar
		let c = from_str("3M2D2M").unwrap();

		// Cross the deletion in the query.
		assert_eq!( offset_in_ref(&c,2),   Ok(2) );
		assert_eq!( offset_in_ref(&c,3),   Ok(5) );

		// Cross the deletion in the ref.
		assert_eq!( offset_in_query(&c,2), Ok(2) );
		assert_eq!( offset_in_query(&c,3), Err(Some(OP_D)) );
		assert_eq!( offset_in_query(&c,4), Err(Some(OP_D)) );
		assert_eq!( offset_in_query(&c,5), Ok(3) );
	}

	#[test]
	fn one_insertion() {

		//    012 345 Offset in ref (from alignment POS, not ref beginning!)
		//    ||| |||
		//    0123456 Offset in query
		//    MMMIMMM Cigar
		let c = from_str("3M1I3M" ).unwrap();

		// Cross the insertion in the query.
		assert_eq!( offset_in_ref(&c,2),   Ok(2) );
		assert_eq!( offset_in_ref(&c,3),   Err(Some(OP_I)) );
		assert_eq!( offset_in_ref(&c,4),   Ok(3) );

		// Cross the insertion in the ref.
		assert_eq!( offset_in_query(&c,2), Ok(2) );
		assert_eq!( offset_in_query(&c,3), Ok(4) );
	}

	#[test]
	fn hard_and_soft_padding() {

		//      012 Offset in ref (from alignment POS, not ref beginning!)
		//      |||
		//    01234 Offset in query
		//   HSSMMM Cigar
		let c = from_str("1H2S3M" ).unwrap();

		assert_eq!( offset_in_ref(&c,0),   Err(Some(OP_S)) );
		assert_eq!( offset_in_ref(&c,1),   Err(Some(OP_S)) );
		assert_eq!( offset_in_ref(&c,2),   Ok(0) );
		assert_eq!( offset_in_ref(&c,3),   Ok(1) );

		assert_eq!( offset_in_query(&c,0), Ok(2) );
		assert_eq!( offset_in_query(&c,1), Ok(3) );
		assert_eq!( offset_in_query(&c,2), Ok(4) );
	}

	#[test]
	fn three_indels() {

		//     0 123456  7 Offset in ref
		//     | ||  ||  |
		//    01234  56789 Offset in query
		//    SMIXXDDMMII= Cigar
		let c = from_str("1S1M1I2X2D2M2I1=" ).unwrap();

		assert_eq!( offset_in_ref(&c,0),   Err(Some(OP_S)) );
		assert_eq!( offset_in_ref(&c,1),   Ok(0) );
		assert_eq!( offset_in_ref(&c,2),   Err(Some(OP_I)) );
		assert_eq!( offset_in_ref(&c,3),   Ok(1) );
		assert_eq!( offset_in_ref(&c,4),   Ok(2) );
		assert_eq!( offset_in_ref(&c,5),   Ok(5) );
		assert_eq!( offset_in_ref(&c,6),   Ok(6) );
		assert_eq!( offset_in_ref(&c,7),   Err(Some(OP_I)) );
		assert_eq!( offset_in_ref(&c,8),   Err(Some(OP_I)) );
		assert_eq!( offset_in_ref(&c,9),   Ok(7) );

		assert_eq!( offset_in_query(&c,0), Ok(1) );
		assert_eq!( offset_in_query(&c,1), Ok(3) );
		assert_eq!( offset_in_query(&c,2), Ok(4) );
		assert_eq!( offset_in_query(&c,3), Err(Some(OP_D)) );
		assert_eq!( offset_in_query(&c,4), Err(Some(OP_D)) );
		assert_eq!( offset_in_query(&c,5), Ok(5) );
		assert_eq!( offset_in_query(&c,6), Ok(6) );
		assert_eq!( offset_in_query(&c,7), Ok(9) );
	}

	#[test]
	#[should_panic]
	fn bad_opcode() {
		let _c = from_str("1H1S3B" ).unwrap();
	}
}

