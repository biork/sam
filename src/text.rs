
use std::str::FromStr;
use std::fmt;
use std::fmt::Display;

use crate::Goff;
use crate::Gpos;
use crate::Alignment;
use crate::cigar;

/// Represents a single-end or one end of a paired-end alignment.
///
/// Implements a subset of the [spec](https://github.com/samtools/hts-specs).
///
/// # Implementation Notes
/// Most of Record's fields are public to avoid an indirection through the
/// Alignment trait's methods, and _the fields have names identical to the
/// trait functions_! A caller holding a text.Record should not need the
/// corresponding trait method, but if they want it, it is available
/// though trait [disambiguation](https://doc.rust-lang.org/book/ch19-03-advanced-traits.html#fully-qualified-syntax-for-disambiguation-calling-methods-with-the-same-name).

pub struct Record {
	pub qname: String,
	pub flagbits: u16,
	pub rname: String,
	pub pos:   Gpos,
	pub mapq:  u8,
	pub alignment: Vec<cigar::Op>,
	pub rnext: String,
	pub pnext: Goff,
	pub tlen:  i32,
	pub seq:   Vec<u8>,
	pub qual:  Vec<u8>
}


impl Alignment for Record {

	fn cigar( &self ) -> &[cigar::Op] {
		self.alignment.as_slice() //iter()
	}

	fn endpos( &self ) -> Gpos {
		self.pos + self.ref_base_count() - 1
	}

	fn ref_position_of( &self, query_offset : u32 ) -> Result<Goff,Option<u8>> {

		assert!( (query_offset as usize) < self.seq.len() );

		cigar::offset_in_ref( self.cigar(), query_offset )
			.map( |off| self.pos + off as Gpos )
	}

	fn query_offset_of( &self, ref_position : Gpos ) -> Result<u32,Option<u8>> {

		assert!( ref_position >= self.pos );

		cigar::offset_in_query( self.cigar(), ref_position - self.pos )
	}

	fn query_slice( &self, _off : u32, _len : u32 ) -> Vec<u8> {
		unimplemented!();
	}

	fn base_at_query_offset( &self, query_offset : u32 ) -> u8 {

		assert!( (query_offset as usize) < self.seq.len() );
		self.seq[ query_offset as usize ]
	}

	fn qual_at_query_offset( &self, query_offset : u32 ) -> u8 {

		assert!( (query_offset as usize) < self.qual.len() );
		self.qual[ query_offset as usize ]
	}

	fn base_at_ref_position( &self, ref_position : Gpos ) -> Result<u8,Option<u8>> {

		if ref_position >= self.pos {
			cigar::offset_in_query( self.cigar(), ref_position - self.pos )
				.map( |oiq| self.base_at_query_offset( oiq ) )
		} else {
			Err(None)
		}
	}

	fn qual_at_ref_position( &self, ref_position : Gpos ) -> Result<u8,Option<u8>> {

		if ref_position >= self.pos {
			cigar::offset_in_query( self.cigar(), ref_position - self.pos )
				.map( |oiq| self.qual_at_query_offset( oiq ) )
		} else {
			Err(None)
		}
	}
}


#[derive(Debug)]
pub enum SamFromStringError {
	Parse(std::num::ParseIntError),
	BadCigar
}

impl From<std::num::ParseIntError> for SamFromStringError {
	fn from( err:std::num::ParseIntError ) -> Self {
		SamFromStringError::Parse(err)
	}
}


impl FromStr for Record {

	type Err = SamFromStringError;

	fn from_str( s: &str ) -> Result<Self,Self::Err> {

		debug_assert!( s.is_ascii() ); // NOTE: BAM files should ONLY contain ASCII!

		let fields:Vec<&str> = s.trim().split('\t').collect();

		let flagbits : u16 = fields[1].parse()?;
		let pos : Goff = fields[3].parse()?;
		let mapq : u8  = fields[4].parse()?;
		let cigar = cigar::from_str( fields[5] )
			.ok_or( SamFromStringError::BadCigar )?;

		let pnext = fields[7].parse()?;
		let tlen  = fields[8].parse()?;

		let rec = Record {
			qname:fields[0].to_string(),
			flagbits,
			rname:fields[2].to_string(),
			pos,
			mapq,
			alignment:cigar,
			rnext:fields[6].to_string(),
			pnext:pnext,
			tlen:tlen,
			// NOTE: samtools view <BAM> *should* be ASCII, so as_bytes is safe!
			seq:fields[9].as_bytes().to_vec(),
			qual:fields[10].as_bytes().to_vec()
			//extra:parsed_extra
		};

		assert!( rec.cigar().len() == 0 || rec.query_base_count() == rec.seq.len() );

		Ok(rec)
	}
}


impl Display for Record {

	fn fmt( &self, f: &mut fmt::Formatter) -> fmt::Result {

		write!( f, "{0:}\t{1:}\t{2:}\t{3:}\t{4:}\t",
			self.qname,
			self.flagbits,
			self.rname,
			self.pos,
			self.mapq
			)?;

		for op in &self.alignment {
			write!( f, "{}", op )?
		}

		unsafe {
			write!( f, "{0:}\t{1:}\t{2:}\t{3:}\t{4:}\t",
				self.rnext,
				self.pnext,
				self.tlen,
				String::from_utf8_unchecked( self.seq.to_vec() ),
				String::from_utf8_unchecked( self.qual.to_vec() )
				)
		}
	}
}


#[cfg(test)]
mod tests {

  use super::*;
	use crate::cigar::{OP_D,OP_I};

	// Some of these tests are just Alignment's thin wrappers around the
	// cigar functions:
	// 1. cigar::offset_in_ref		-> Alignment::ref_position_of
	// 2. cigar::offset_in_query  -> Alignment::query_offset_of
	// All these really do is account for the offset/position distinction.

	#[test]
	fn one_deletion() {

		//    0123456 Offset in ref (from alignment POS, not ref beginning!)
		//    |||  ||
		//    012  34 Offset in query
		//    MMMDDMM Cigar
		let a = Record::from_str(
			"readid\t64\tref\t7\t255\t3M2D2M\t*\t0\t0\tACTGA\tJJJJJ" ).unwrap();

		// Cross the deletion in the query.
		assert_eq!( a.ref_position_of(2),  Ok(7+2) );
		assert_eq!( a.ref_position_of(3),  Ok(7+5) );

		// Cross the deletion in the ref.
		assert_eq!( a.query_offset_of(7+2), Ok(2) );
		assert_eq!( a.query_offset_of(7+3), Err(Some(OP_D)) );
		assert_eq!( a.query_offset_of(7+4), Err(Some(OP_D)) );
		assert_eq!( a.query_offset_of(7+5), Ok(3) );
	}

	#[test]
	fn one_insertion() {

		//    901 345  Position in ref 
		//    012 345  Offset in ref (from alignment POS, not ref beginning!)
		//    ||| |||
		//    0123456  Offset in query
		//    MMMIMMM  Cigar
		let a = Record::from_str(
			"readid\t64\tref\t9\t255\t3M1I3M\t*\t0\t0\tACTGACT\tJJJJJJJ" ).unwrap();

		// Cross the insertion in the query.
		assert_eq!( a.ref_position_of(2),   Ok(9+2) );
		assert_eq!( a.ref_position_of(3),   Err(Some(OP_I)) );
		assert_eq!( a.ref_position_of(4),   Ok(9+3) );

		// Cross the insertion in the ref.
		assert_eq!( a.query_offset_of(9+2), Ok(2) );
		assert_eq!( a.query_offset_of(9+3), Ok(4) );
	}

	#[test]
	fn three_indels() {

		//     3 456789  0  Position in ref 
		//     0 123456  7  Offset in ref
		//     | ||  ||  |
		//    01234  56789  Offset in query
		//    SMIXXDDMMII=  Cigar
		//    ACTGG??TCACT
		let a = Record::from_str(
			"readid\t64\tref\t3\t255\t1S1M1I2X2D2M2I1=\t*\t0\t0\tACTGGTCACT\tJJJJJJJJJJ" ).unwrap();

		assert_eq!(a.base_at_ref_position( 2 ), Err(None) ); 
		assert_eq!(a.base_at_ref_position( 3 ), Ok('C' as u8) ); 
		// insertion
		assert_eq!(a.base_at_ref_position( 4 ), Ok('G' as u8) ); 
		assert_eq!(a.base_at_ref_position( 5 ), Ok('G' as u8) ); 
		assert_eq!(a.base_at_ref_position( 6 ), Err(Some(OP_D)) ); 
		assert_eq!(a.base_at_ref_position( 7 ), Err(Some(OP_D)) ); 
		assert_eq!(a.base_at_ref_position( 8 ), Ok('T' as u8) ); 
		assert_eq!(a.base_at_ref_position( 9 ), Ok('C' as u8) ); 
		assert_eq!(a.base_at_ref_position( 10 ), Ok('T' as u8) ); 

		// Cross the 1st insertion in the query.
		assert_eq!( a.ref_position_of(1),   Ok(3+0) );
		assert_eq!( a.ref_position_of(2),   Err(Some(OP_I)) );
		assert_eq!( a.ref_position_of(3),   Ok(3+1) );
	}
}

