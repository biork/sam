
//! Constants and bool-returning bit testers for interpretation of the SAM
//! (_and_ BAM) formats' 16-bit flags field.

/// M template having multiple segments in sequencing
const MULT:u16 =   0x1;
/// A each segment properly aligned according to the aligner
const ALGN:u16 =   0x2;
/// U segment unmapped
const UNMP:u16 =   0x4;
/// u next segment in the template unmapped
const NUNM:u16 =   0x8;
/// R SEQ being reverse complemented
const REVC:u16 =  0x10;
/// r SEQ of the next segment in the template being reverse complemented
const NREV:u16 =  0x20;
/// < the first segment in the template
const FRST:u16 =  0x40;
/// > the last segment in the template
const LAST:u16 =  0x80;
/// 2 secondary alignment
const SECD:u16 = 0x100;
/// F not passing filters, such as platform/vendor quality controls
const FAIL:u16 = 0x200;
/// D PCR or optical duplicate
const ODUP:u16 = 0x400;
/// S supplementary alignment
const SUPP:u16 = 0x800;

pub fn is_multiseg( val:u16 )   -> bool { val & MULT != 0 }
pub fn is_aligned( val:u16 )    -> bool { val & ALGN != 0 }

pub fn this_unmapped( val:u16 ) -> bool { val & UNMP != 0 }
pub fn next_unmapped( val:u16 ) -> bool { val & NUNM != 0 }

pub fn this_revcomp( val:u16 )  -> bool { val & REVC != 0 }
pub fn next_revcomp( val:u16 )  -> bool { val & NREV != 0 }

pub fn is_first( val:u16 )      -> bool { val & FRST != 0 }
pub fn is_last( val:u16 )       -> bool { val & LAST != 0 }

pub fn is_secondary( val:u16 )  -> bool { val & SECD != 0 }
pub fn is_failure( val:u16 )    -> bool { val & FAIL != 0 }
pub fn is_duplicate( val:u16 )  -> bool { val & ODUP != 0 }
pub fn is_supp( val:u16 )       -> bool { val & SUPP != 0 }

pub fn is_extra( val:u16 ) -> bool { val & 0xF00 != 0 }

//const SYMBOLIC_FLAGS : [char;12] = [
//	'M','A','U','u','R','r','<','>','2','F','D','S'];

/*
impl Display for Flags {
	// TODO: Implement a formatting code for "pretty" printing.
	fn fmt( val:u16, f: &mut fmt::Formatter) -> fmt::Result {
		for i in 0..12 {
			write!( f, "{}", if ((1<<i) & val) != 0 { SYMBOLIC_FLAGS[i] } else {'_'} )?
		}
		Ok(())
	}
}
*/

