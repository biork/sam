# sam

Parses SAM file records--that is, _text_-based alignment records and
provides for interpretation of CIGAR strings.

* Ignores SAM file headers ("@"-prefixed lines)
* Ignores the optional _tags_ in SAM lines--that is, fields after the 11th
	("QUAL") field.
* Does not currently _write_ SAM files, only reads.
* Does not read BAM files.

No indeed, this package does not do terribly much yet...accept isolating
SAM reading to one place. The most valuable part of this library currently
its CIGAR string handling which is non-trivial.
