1.QNAME: Query template NAME. Reads/segments having identicalQNAMEare 
regarded to come fromthe same template.  AQNAME‘*’ indicates the 
information is unavailable.  In a SAM file, a read mayoccupy multiple 
alignment lines, when its alignment is chimeric or when multiple mappings 
are given.

2.FLAG: Combination of bitwise FLAGs.10Each bit is explained in 
the following table

• For each read/contig in a SAM file, it is required that one and 
only one line associated with theread satisfies ‘FLAG& 0x900 == 0’.  
This line is called theprimary lineof the read.
• Bit 0x100 marks the 
alignment not to be used in certain analyses when the tools in use are 
awareof this bit.  It is typically used to flag alternative mappings when 
multiple mappings are presentedin a SAM.
• Bit 0x800 indicates that the 
corresponding alignment line is part of a chimeric alignment.  A 
lineflagged with 0x800 is called as asupplementary line.
• Bit 0x4 is the only reliable place to tell whether the read is unmapped. If 0x4 is set, no 
assumptionscan be made aboutRNAME,POS,CIGAR,MAPQ, and bits 0x2, 0x100, and 
0x800.

• Bit  0x10  indicates  whetherSEQhas  been  reverse  complemented  
andQUALreversed.   Whenbit 0x4 is unset, this corresponds to the strand to 
which the segment has been mapped:  bit 0x10unset  indicates  the  forward  
strand,  while  set  indicates  the  reverse  strand.   When  0x4  is  
set,this indicates whether the unmapped read is stored in its original 
orientation as it came off thesequencing machine.

• Bits  0x40  and  0x80  reflect  the  read  ordering  within  each  template  inherent  in  the  
sequencingtechnology used.11If 0x40 and 0x80 are both set, the read is part 
of a linear template, but it isneither the first nor the last read.  If 
both 0x40 and 0x80 are unset, the index of the read in thetemplate is 
unknown.  This may happen for a non-linear template or when this 
information is lostduring data processing.•If 0x1 is unset, no 
assumptions can be made about 0x2, 0x8, 0x20, 0x40 and 0x80.
• Bits that are not listed in the table are reserved for future use.  They should not 
be set whenwriting and should be ignored on reading by current 
software.

3.RNAME: Reference sequence NAME of the alignment.  If@SQheader 
lines are present,RNAME(if not‘*’) must be present in one of 
theSQ-SNtag.  An unmapped segment without coordinate has a ‘*’ atthis 
field.  However, an unmapped segment may also have an ordinary coordinate 
such that it can beplaced at a desired position after sorting.  IfRNAMEis 
‘*’, no assumptions can be made aboutPOSandCIGAR.
4.POS: 1-based 
leftmost mapping POSition of the firstCIGARoperation that “consumes” a 
referencebase (see table below).  The first base in a reference sequence 
has coordinate 1.POSis set as 0 foran unmapped read without coordinate.  
IfPOSis 0, no assumptions can be made aboutRNAMEandCIGAR.
5.MAPQ: MAPping 
Quality.  It equals−10 log10Pr{mapping position is wrong}, rounded to the 
nearestinteger.  A value 255 indicates that the mapping quality is not 
available.
6.CIGAR: CIGAR string.  The CIGAR operations are given in the 
following table (set ‘*’ if 
unavailable):OpBAMDescriptionConsumesqueryConsumesreferenceM0alignment 
match (can be a sequence match or mismatch)yesyesI1insertion to the 
referenceyesnoD2deletion from the referencenoyesN3skipped region from the 
referencenoyesS4soft clipping (clipped sequences present inSEQ)yesnoH5hard 
clipping (clipped sequences NOT present inSEQ)nonoP6padding (silent 
deletion from padded reference)nono=7sequence matchyesyesX8sequence 
mismatchyesyes•“Consumes query” and “consumes reference” indicate 
whether the CIGAR operation causes thealignment to step along the query 
sequence and the reference sequence respectively.•Hcan only be present as 
the first and/or last operation.•Smay only haveHoperations between them 
and the ends of theCIGARstring.•For mRNA-to-genome alignment, 
anNoperation represents an intron.  For other types of align-ments, the 
interpretation ofNis not defined.•Sum of lengths of 
theM/I/S/=/Xoperations shall equal the length ofSEQ.11For example, in 
Illumina paired-end sequencing,first(0x40) corresponds to the R1 
‘forward’ read andlast(0x80) to theR2  ‘reverse’  read.   (Despite  
the  terminology,  this  is  unrelated  to  the  segments’  orientations  
when  they  are  mapped:  either,neither, or both may have theirreverseflag 
bits (0x10) set after mapping.)7

7.RNEXT: Reference sequence name of the primary alignment of the NEXT read 
in the template.  Forthe last read, the next read is the first read in the 
template.  If@SQheader lines are present,RNEXT(ifnot ‘*’ or ‘=’) 
must be present in one of theSQ-SNtag.  This field is set as ‘*’ when 
the information isunavailable, and set as ‘=’ ifRNEXTis identicalRNAME. 
If not ‘=’ and the next read in the templatehas one primary mapping 
(see also bit 0x100 inFLAG), this field is identical toRNAMEat the 
primaryline of the next read.  IfRNEXTis ‘*’, no assumptions can be 
made onPNEXTand bit 0x20.
8.PNEXT: 1-based Position of the primary alignment 
of the NEXT read in the template.  Set as 0 whenthe information is 
unavailable.  This field equalsPOSat the primary line of the next read.  
IfPNEXTis 0, no assumptions can be made onRNEXTand bit 0x20.
9.TLEN:  signed 
 observed  Template  LENgth.   If  all  segments  are  mapped  to  the  
same  reference,  theunsigned observed template length equals the number of 
bases from the leftmost mapped base to therightmost mapped base.  The 
leftmost segment has a plus sign and the rightmost has a minus sign.The 
sign of segments in the middle is undefined.  It is set as 0 for 
single-segment template or when theinformation is unavailable.
10.SEQ: 
segment SEQuence.  This field can be a ‘*’ when the sequence is not 
stored.  If not a ‘*’, the lengthof the sequence must equal the sum of 
lengths ofM/I/S/=/Xoperations inCIGAR. An ‘=’ denotes thebase is 
identical to the reference base.  No assumptions can be made on the letter 
cases.
11.QUAL: ASCII of base QUALity plus 33 (same as the quality string in 
the Sanger FASTQ format).  Abase quality is the phred-scaled base error 
probability which equals−10 log10Pr{base is wrong}.  Thisfield can be a 
‘*’ when quality is not stored.  If not a ‘*’,SEQmust not be a 
‘*’ and the length of thequality string ought to equal the length ofSEQ.
